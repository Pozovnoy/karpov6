# syntax=docker/dockerfile:1
FROM python:3.9-alpine
RUN mkdir /app
RUN pip install --upgrade pip
ENV FLASK_RUN_HOST=0.0.0.0
ADD requirements.txt /app
ADD app.py /app
WORKDIR /app
RUN pip3 install -r requirements.txt


CMD [ "python3", "app.py"]
