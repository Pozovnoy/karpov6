import requests
import flask
from flask import request

import os

import mlflow

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.109.9.128:9000"
os.environ["MLFLOW_TRACKING_URL"] = "http://65.109.9.128:6000"
os.environ["AWS_ACCESS_KEY_ID"] ="IAM_ACCESS_KEY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"


mlflow.set_tracking_uri("http://65.109.9.128:6000")
mlflow.set_experiment("karpov9")


client = mlflow.tracking.MlflowClient()

model_float_path = "models:/float_model/production"
model_string_path = "models:/string_model/production"

model_string = mlflow.pyfunc.load_model(model_string_path)
model_float = mlflow.sklearn.load_model(model_float_path)

##########################
# Создаем FLask приложение
##########################

app = flask.Flask(__name__)


# /get_source_iris_pred?sepal_length=<float>&sepal_width=<float>
@app.route('/get_source_iris_pred')
def get_source_iris_pred():
    length = request.args.get('sepal_length')
    width = request.args.get('sepal_width')
    prediction = model_float.predict([[float(length), float(width)]])
    print(prediction)
    return flask.jsonify({'prediction': prediction[0]})



@app.route('/get_string_iris_pred')
def get_string_iris_pred():
    sepal_length = request.args.get("sepal_length")
    sepal_width = request.args.get("sepal_width")

    prediction = model_string.predict([float(sepal_length), float(sepal_width)])
    return flask.jsonify(prediction)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5100)

